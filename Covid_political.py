# -*- coding: utf-8 -*-
"""
Created on Mon Jan  4 21:21:48 2021

@author: Andrew
"""

import pandas as pd
import matplotlib.pyplot as plt 
from sklearn import preprocessing, linear_model
import numpy as np

import seaborn as sns
df=pd.read_csv("C:\\Users\\Andrew\\Documents\\Covid-Panda\\Political_Data\\HoC-GE2019-results-by-constituency-csv.csv")
df2=pd.read_csv("C:\\Users\\Andrew\\Documents\\Covid-Panda\\Political_Data\\Case_HB.csv")



df.drop(["ons_id","ons_region_id"],inplace=True ,axis=1)
scotdata=df[df["country_name"] == "Scotland"]




aberdeen=scotdata[scotdata["constituency_name"].str.contains("Aberdeen")]


hb_ggc=("Glasgow Central","Glasgow East","Glasgow North","Glasgow North East","Glasgow North West"
        ,"Glasgow South","Glasgow South West","East Renfrewshire","Inverclyde","East Dunbartonshire"
        ,"West Dunbartonshire","Paisley and Renfrewshire North","Paisley and Renfrewshire South")

hb_aa=("Ayr, Carrick and Cumnock","Central Ayrshire","Kilmarnock and Loudoun","North Ayrshire and Arran")

hb_dg=("Dumfries and Galloway","Dumfriesshire, Clydesdale and Tweeddale")


hb_lan=("Motherwell and Wishaw","Rutherglen and Hamilton West","Lanark and Hamilton East","Cumbernauld, Kilsyth and Kirkintilloch East",
        "Coatbridge, Chryston and Bellshill","Airdrie and Shotts","East Kilbride, Strathaven and Lesmahagow")

hb_lot=("Linlithgow and East Falkirk","East Lothian","Midlothian","Livingston","Edinburgh East"
        ,"Edinburgh North and Leith","Edinburgh South","Edinburgh South West","Edinburgh West")

hb_tay=("Dundee East","Dundee West","Ochil and South Perthshire","Perth and North Perthshire","Angus")

hb_fif=("Kirkcaldy and Cowdenbeath","North East Fife","Glenrothes")

hb_fv =("Falkirk","Stirling") 

hb_grm = ("Moray","Aberdeen North","Aberdeen South","Banff and Buchan","West Aberdeenshire and Kincardine")

hb_hig =("Argyll and Bute","Caithness, Sutherland and Easter Ross","Inverness, Nairn, Badenoch and Strathspey","Ross, Skye and Lochaber")

hb_bor = ("Berwickshire, Roxburgh and Selkirk","Gordon")
hb_wi =("Na h-Eileanan An Iar")

hb_ork = ("Orkney and Shetland")

#~~~~~~~~
#Searches for health board names matching list of constituencies
#~~~~~~~~
scotdata["hb_name"]=scotdata["constituency_name"]
scotdata["hb_name"]=scotdata["hb_name"].replace(hb_ggc,"NHS Greater Glasgow & Clyde")

scotdata["hb_name"]=scotdata["hb_name"].replace(hb_aa,"NHS Ayrshire & Arran")
scotdata["hb_name"]=scotdata["hb_name"].replace(hb_dg,"NHS Dumfries & Galloway")
scotdata["hb_name"]=scotdata["hb_name"].replace(hb_lan,"NHS Lanarkshire")
scotdata["hb_name"]=scotdata["hb_name"].replace(hb_lot,"NHS Lothian")
scotdata["hb_name"]=scotdata["hb_name"].replace(hb_tay,"NHS Tayside")
scotdata["hb_name"]=scotdata["hb_name"].replace(hb_fif,"NHS Fife")
scotdata["hb_name"]=scotdata["hb_name"].replace(hb_fv,"NHS Forth Valley")
scotdata["hb_name"]=scotdata["hb_name"].replace(hb_grm,"NHS Grampian")
scotdata["hb_name"]=scotdata["hb_name"].replace(hb_hig,"NHS Highland")
scotdata["hb_name"]=scotdata["hb_name"].replace(hb_bor,"NHS Borders")
scotdata["hb_name"]=scotdata["hb_name"].replace(hb_ork,"NHS Orkney")

#~~~~~~~~
#Filters glasgow only data 
#~~~~~~~~
glasgow_data= scotdata[scotdata["hb_name"].str.contains("NHS Greater Glasgow & Clyde")]

#Here the strength of the majority for each glasgow district is found 
glasgow_data["majority_strength"]=glasgow_data["majority"]/glasgow_data["valid_votes"]



#Covid data movement
sd_hb =scotdata.sort_values(["hb_name"])
#extracts only entries where they have been tagged with a healthboard  
sd_hb=sd_hb[sd_hb["hb_name"].str.startswith('NHS')]


hbnames=["NHS Greater Glasgow & Clyde","NHS Ayrshire & Arran","NHS Dumfries & Galloway",
         "NHS Lanarkshire","NHS Lothian","NHS Tayside",
         "NHS Fife","NHS Forth Valley","NHS Grampian",
         "NHS Highland","NHS Borders","NHS Orkney"]

sd_hb["covidcases_norm"] = sd_hb["pc"]

# This loop matches heath board cases to voting district results and inserts a 
# value for cases normalized
for hb in hbnames :
    location=np.where(df2["HBName"]==hb)[0]
    #gad=np.where(sd_hb["hb_name"]==hb)
    #print gad
    cases=df2.iloc[location,7][location]
    print cases
    sd_hb["covidcases_norm"] = np.where((sd_hb["hb_name"]==hb),cases,sd_hb.covidcases_norm)
    #sd_hb["covidcases_norm"] = np.where((sd_hb["hb_name"]==hb),cases,sd_hb.covidcases_norm)





#Glasgow only data prep
features_glasgow = glasgow_data.loc[:,"electorate":"majority_strength"]
features_glasgow =features_glasgow.drop("hb_name",axis =1)
scale_data = preprocessing.StandardScaler().fit(features_glasgow)
features_glasgow_scaled=scale_data.transform(features_glasgow)




#scotland wide data prep


#scotdata.set_index(")
features_scotdata = scotdata.loc[:,"electorate":"other"]

scale_data_scot = preprocessing.StandardScaler().fit(features_scotdata)

features_scot_scaled  = scale_data_scot.transform(features_scotdata)

features_scot_scaled_df= pd.DataFrame(features_scot_scaled, index=features_scotdata.index,columns =features_scotdata.columns )
label = scotdata.loc[:,"constituency_name":"second_party"]




#parties =scotdata.groupby("first_party")
df4 =pd.merge(label,features_scot_scaled_df, left_index=True,right_index=True)
final_recom =pd.merge(label,features_scot_scaled_df, left_index=True,right_index=True)
parties = final_recom.groupby("first_party")
#final_recom.reset_index(inplace=True)
print parties
#plotting = pd.DataFrame(dict(x=maj, y=elec, label=parties))
#parties =plotting.groupby("first_party")





###########
fig, ax1 = plt.subplots()

meantest=final_recom["electorate"].agg("mean")
for name, party in parties:
        
        regr = linear_model.LinearRegression()
        regr.fit(party["majority"].values[:,np.newaxis], party["electorate"])
        if name=="SNP" :
            ax1.scatter(party["majority"],party["electorate"],label=name,c="y")
            final_recom.set_index("majority", inplace=True)
            predic_raw = regr.predict(party["majority"].values[:,np.newaxis])
            prediction=pd.DataFrame(predic_raw, index=party["majority"])
            
            
            plt.plot(prediction)
        elif name=="Con" :
            ax1.scatter(party["majority"],party["electorate"],label=name,c="blue")
        elif name=="Lab":
            ax1.scatter(party["majority"],party["electorate"],label=name,c="red") 
        else:
        
            ax1.scatter(party["majority"],party["electorate"],label=name,c="green")

plt.figure(figsize=(10,5))


fig2, bx1 = plt.subplots()

#bx1.hist(final_recom["first_party"])#,df2["TotalCases"])


#fig3=sns.countplot(final_recom["first_party"],color = "coral")



data_sns = [df4["majority"],df4["result"],df4["mp_gender"],df4["first_party"], df4["electorate"],df4["snp"]]
headers = ["maj","result","gender","party", "elec","snp"]


df3 = pd.concat(data_sns, axis=1, keys=headers)
df3.reset_index(drop=True)

fig4= sns.stripplot(x=sd_hb["first_party"],y=sd_hb["covidcases_norm"])
print "Bam"

##old code
"""
#plt.scatter(glasgow_data["majority_strength"],glasgow_data["result"])
#print scotdata["con"][scotdata["result"].str.contains("SNP")]

## Exporting data
#scotdata.to_csv("C:\\Users\\Andrew\\Documents\\Covid-Panda\\Political_Data\\Scottish_election.csv")

#print scotdata
#scotdata["first_party"].plot.hist(bins=20)
#scotdata.groupby("first_party")["majority","snp","con","lab","ld"].mean().plot(kind='bar')



#ax1.axhline()
#
#ax1 = scotdata.plot.scatter(x="majority",y="electorate",c="result")
#plt.scatter(features_scot_scaled[:,0],features_scot_scaled[:,3])
"""