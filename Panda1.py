# -*- coding: utf-8 -*-
"""
Created on Thu Dec 17 21:21:36 2020

@author: Andrew
"""
import pandas as pd
import matplotlib.pyplot as plt 


df=pd.read_excel("C:\\Users\\Andrew\\Documents\\Covid-Panda\\2020-11-18_COVID-19_Press_Conference_Data.xlsx",sheet_name="Mobility, UK")
newnames = {"Timing in pS":"time","Pseudo Torch charge in pC":"ptorch","Real Laser Torch charge in pC":"rltorch","Trojan Horse charge in pC":"trojan"}
df_working=df.iloc[2:53,1:7]
df_working.columns = df_working.iloc[0]

df_working.reset_index(drop=True,inplace=True)
df_working=df_working.iloc[1:53]
df_working=df_working.replace([':'],'')
df_working.iloc[:,2:].astype(float)
#df=df_working[:,2:].astype(float)
#print df_working["Parks"]
df_working2= df_working.iloc[:,1:]

df_working2.plot.hist(bins=20)
